import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class EasyTest {
    private WebDriver driver;
    private String baseUrl = "https://www.seleniumeasy.com";

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get(baseUrl);
        driver.manage().window().maximize();
    }

    @Test(enabled = false)
    public void radioButtonTest() {
        driver.get(baseUrl + "/test/basic-radiobutton-demo.html");
        String message = "Radio button 'Female' is checked";
        WebElement maleRadioButton = driver.findElement(By.cssSelector("input[type='radio'][value='Male'][name='optradio']"));
        WebElement femaleRadioButton = driver.findElement(By.cssSelector("input[type='radio'][value='Female'][name='optradio']"));
        WebElement buttonCheck = driver.findElement(By.cssSelector("button[type='button'][id='buttoncheck']"));
        femaleRadioButton.click();
        buttonCheck.click();

        //Check expected behaviour
        String actualMessage = driver.findElement(By.cssSelector("p[class='radiobutton']")).getText();
        Assert.assertEquals(message, actualMessage, "The expected message was not correct");
    }


    @Test(enabled = false)
    public void radioButtonTest_multiSelect() {
        //TODO
    }

    @Test(enabled = false)
    public void dropDownTest() throws InterruptedException {
        driver.get(baseUrl + "/test/basic-select-dropdown-demo.html");
        WebElement elementSelect = driver.findElement(By.id("select-demo"));
        Select listSelect = new Select(elementSelect);
        listSelect.selectByValue("Sunday");
        System.out.println("Selected option by value is: " + listSelect.getFirstSelectedOption().getText());
        Thread.sleep(2000);
        listSelect.selectByIndex(6);
        System.out.println("Selected option by index is: "+ listSelect.getFirstSelectedOption().getText());
    }

    @Test(enabled = false)
    public void dropDownTest_multiSelect() throws InterruptedException{
        driver.get(baseUrl + "/test/basic-select-dropdown-demo.html");
        WebElement elementMultiSelect = driver.findElement(By.id("multi-select"));
        Select listSelect = new Select(elementMultiSelect);
        listSelect.selectByValue("Florida");
        listSelect.selectByValue("Texas");
        listSelect.selectByValue("New York");
        Thread.sleep(2000);

        List<WebElement> selectList = listSelect.getAllSelectedOptions();

        for (WebElement selectedElement: selectList) {
            System.out.println("Selected element: " + selectedElement.getText());
        }

        listSelect.deselectByValue("New York");

        selectList = listSelect.getAllSelectedOptions();

        for (WebElement selectedElement: selectList) {
            System.out.println("Selected element: " + selectedElement.getText());
        }
    }

    //Alerts Management

    @Test(enabled = false)
    public void alert_accept() throws InterruptedException {
        driver.get(baseUrl + "/test/javascript-alert-box-demo.html");
        driver.findElement(By.cssSelector("button[class='btn btn-default']")).click();
        Thread.sleep(3000);
        driver.switchTo().alert().accept();
    }

    @Test(enabled = false)
    public void alert_cancel() throws InterruptedException {
        driver.get(baseUrl + "/test/javascript-alert-box-demo.html");
        driver.findElement(By.xpath("//button[@class='btn btn-default btn-lg'] [text()='Click me!']")).click();
        Thread.sleep(3000);
        driver.switchTo().alert().dismiss();
    }

    @Test(enabled = false)
    public void alert_setMessage() throws InterruptedException{
        String expectedMessage = "You have entered 'Hello Selenium Web driver' !";
        driver.get(baseUrl + "/test/javascript-alert-box-demo.html");
        driver.findElement(By.xpath("//button[@class='btn btn-default btn-lg'] [text()='Click for Prompt Box']")).click();
        Thread.sleep(3000);
        Alert alert = driver.switchTo().alert();
        alert.sendKeys("Hello Selenium Web driver");
        Thread.sleep(2000);
        alert.accept();
        Assert.assertEquals(expectedMessage, driver.findElement(By.cssSelector("p[id='prompt-demo']")).getText());
    }

    @Test(enabled = false)
    public void alert_getMessage() throws InterruptedException {
        driver.get(baseUrl + "/test/javascript-alert-box-demo.html");
        driver.get(baseUrl + "/test/javascript-alert-box-demo.html");
        driver.findElement(By.xpath("//button[@class='btn btn-default btn-lg'] [text()='Click for Prompt Box']")).click();
        Thread.sleep(3000);
        Alert alert = driver.switchTo().alert();
        String text = alert.getText();
        alert.accept();
        System.out.println("The text is: " + text);
    }

    @Test(enabled = false)
    public void alert_getMessage_wait() {
        driver.get(baseUrl + "/test/javascript-alert-box-demo.html");
        driver.get(baseUrl + "/test/javascript-alert-box-demo.html");
        driver.get(baseUrl + "/test/javascript-alert-box-demo.html");
        driver.findElement(By.xpath("//button[@class='btn btn-default btn-lg'] [text()='Click for Prompt Box']")).click();
        WebDriverWait wait = new WebDriverWait(driver, 3);
        Alert alert = wait.until(ExpectedConditions.alertIsPresent());
        String text = alert.getText();
        alert.accept();
        System.out.println("The text is: " + text);

    }

    @AfterClass
    public void close() throws InterruptedException {
        Thread.sleep(3000);
        driver.close();
    }

}
