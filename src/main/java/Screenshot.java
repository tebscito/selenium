import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Screenshot {

    private WebDriver driver;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://www.google.com");
    }

    @Test
    public void screenshootTest() throws Exception {
        //Call take screenshot function
        WebElement searchText = driver.findElement(By.name("q"));
        searchText.sendKeys("SELENIUM");
        this.takeScreenshot(driver, "src/main/resources/screenshots/screenshotTest");

    }

    private void takeScreenshot(WebDriver driver, String path) throws IOException {
        TakesScreenshot scrShot = (TakesScreenshot) driver;

        File scrFile = scrShot.getScreenshotAs(OutputType.FILE);

        //Crear fecha para el nombre
        String fn = new SimpleDateFormat("yyyyMMddhhmm").format(new Date());
        System.out.println(fn);

        //Escribir nombre completo del archivo con la fecha
        StringBuffer finalScreenshot = new StringBuffer();
        finalScreenshot.append(path);
        finalScreenshot.append("-");
        finalScreenshot.append(fn);
        finalScreenshot.append(".jpg");

        File destFile = new File(finalScreenshot.toString());

        FileUtils.copyFile(scrFile, destFile);

    }


}