import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class ParametersIContextTest {
    private WebDriver driver;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.google.com");
    }

    @Test
    public void testFirstMethod(ITestContext context){
        Integer baseValue = this.fillBuyFormPage();
        context.setAttribute("baseValue", baseValue);
    }

    @Test
    @Parameters({"expected_price"})
    public void testSecondMethod(ITestContext context, Integer expected_price){
        Integer taxValue = this.confirmBuyPage();
        Integer baseValue = (Integer) context.getAttribute("baseValue");
        Integer total = taxValue + baseValue;
        Assert.assertEquals(total, expected_price);
    }

    private Integer fillBuyFormPage() {
        return 100;
    }

    private Integer confirmBuyPage() {
        return 20;
    }

}

