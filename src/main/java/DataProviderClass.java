import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

public class DataProviderClass {

    @DataProvider
    public static Object[][] getDataFromDataprovider() {
        return new Object[][]{
                {"America", "ARG"},
                {"Auropa", "ESP"},
                {"Asia", "CHIN"}
        };
    }

    @DataProvider(name="SearchProvider")
    public Object[][] getDataFromDataprovider(ITestContext c){
        Object[][] groupArray = null;

        for (String group : c.getIncludedGroups()) {

            if(group.equalsIgnoreCase("A")){   //GRUPO A requiere: label, key

                groupArray = new Object[][] {
                        { "Europa", "ESP" },
                        { "America", "USA" },
                        { "Asia", "RUS" }
                };
            }

            else if(group.equalsIgnoreCase("B"))  //GRUPO B requiere: key
            {
                groupArray = new Object[][] {
                        {  "Canada" },
                        {  "España" },
                        {  "Japon" }
                };
            }
        }
        return groupArray;
    }
}
