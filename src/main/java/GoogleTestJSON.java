import com.google.gson.Gson;
import data.GoogleInformation;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class GoogleTestJSON {

    private WebDriver driver;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://google.com");
    }

    @Test
    public void convertirJSONFromGoogle() {

        String firstButton = driver.findElement(By.cssSelector("input[name='btnK']")).getText();
        String secondButton = driver.findElement(By.cssSelector("input[name='btnI']")).getText();

        System.out.println(firstButton);
        System.out.println(secondButton);

        Gson gson = new Gson();
        GoogleInformation basic = new GoogleInformation();

        basic.setFistButton(firstButton);
        basic.setLastButton(secondButton);

        try (FileWriter writer = new FileWriter("basic_information.json")) {

            gson.toJson(basic, writer);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
