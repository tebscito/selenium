import com.google.gson.Gson;
import data.BasicInformation;
import data.GoogleInformation;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class ParametersTest {

    private WebDriver driver;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://google.com");
    }

    //Test sin parmaetrizar
    @Test
    public void testNoParameter() {
        String label = "America";
        String searchKey = "ARG";

        WebElement searchText = driver.findElement(By.name("q"));
        searchText.sendKeys(searchKey);

        System.out.println("Tu Label es " + label + "y tu searchKey es " + searchKey);

        System.out.println("El valor en google search box es " + searchText.getAttribute("value"));

        Assert.assertTrue(searchText.getAttribute("value").equalsIgnoreCase(searchKey));

    }

    //Test parametrizado con xml
    @Test
    @Parameters({"label", "searchKey"})
    public void testParameterWithXML(String label, String searchKey) {

        WebElement searchText = driver.findElement(By.name("q"));
        searchText.sendKeys(searchKey);

        System.out.println("Tu Label es " + label + "y tu searchKey es " + searchKey);

        System.out.println("El valor en google search box es " + searchText.getAttribute("value"));

        Assert.assertTrue(searchText.getAttribute("value").equalsIgnoreCase(searchKey));

    }

    //Test usando data provider
    @Test(dataProvider = "getDataFromDataprovider")
    public void testDataProvider(String label, String searchKey) {

        WebElement searchText = driver.findElement(By.name("q"));
        searchText.clear();
        searchText.sendKeys(searchKey);

        System.out.println("Tu Label es " + label + "y tu searchKey es " + searchKey);

        System.out.println("El valor en google search box es " + searchText.getAttribute("value"));

        Assert.assertTrue(searchText.getAttribute("value").equalsIgnoreCase(searchKey));

    }

    @DataProvider
    public static Object[][] getDataFromDataprovider() {
        return new Object[][]{
                {"America", "ARG"},
                {"Auropa", "ESP"},
                {"Asia", "CHIN"}
        };
    }

    @AfterClass
    public void close() throws InterruptedException {
        Thread.sleep(3000);
        driver.close();
    }


}
