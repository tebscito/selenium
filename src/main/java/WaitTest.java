import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WaitTest {
    private WebDriver driver;
    private String baseUrl = "https://www.seleniumeasy.com";

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test(enabled = false)
    public void waitTest_implicit() {
        driver.get(baseUrl);
        String title = driver.findElement(By.id("site-name")).getText();
        System.out.println("Title of main page is: " + title);
    }

    @Test(enabled = true)
    public void waitTest_explicit() {
        driver.get(baseUrl);
        String title = driver.findElement(By.id("site-name")).getText();
        System.out.println("Title of main page is: " + title);
        //click testng menu
        driver.findElement(By.cssSelector("li[class='leaf'] a[href='/testng-tutorials']")).click();
        //select first article
        WebDriverWait wait = new WebDriverWait(driver, 5);
        //Wait to be visible
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText("Stacktrace and Screenshot")));
        element.click();

        Assert.assertTrue(driver.getCurrentUrl().contains("retry-stacktrace-and-screenshots-in-report"));
        System.out.println("The current url is" + driver.getCurrentUrl());
    }

    @AfterClass
    public void close() throws InterruptedException {
        Thread.sleep(3000);
        driver.close();
    }

}
