import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class TableTest {
    private WebDriver driver;
    private String baseUrl = "https://www.seleniumeasy.com/test";

     WebElement table;
     WebElement header;
     WebElement body;
     WebElement row;
     List<WebElement> rows;
     List<WebElement> columns;
     WebElement cell;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test(enabled = true)
    public void tableAccessElements(ITestContext context) {
        driver.get(baseUrl + "/table-sort-search-demo.html");

        //Table
        table = driver.findElement(By.cssSelector("table#example")); //css
        table = driver.findElement(By.xpath("//table[@id='example']")); //xpath
        //System.out.println(table.getText());

        //Header
        header = driver.findElement(By.cssSelector("table#example > thead")); //css
        header = driver.findElement(By.xpath("//table[@id='example']/thead")); //xpath
        //System.out.println("HEADER: " + header.getText());

        //Body
        body = driver.findElement(By.cssSelector("table#example > tbody")); //css
        body = driver.findElement(By.xpath("//table[@id='example'] /tbody")); //xpath
        //System.out.println("BODY: " + body.getText());

        //Rows
        rows = driver.findElements(By.cssSelector("table#example > tbody > tr")); //css
        rows = driver.findElements(By.xpath("//table[@id='example']/tbody/tr")); //xpath
        //System.out.println("ROWS: " + rows);

        //Particular Row (5)
        row = driver.findElement(By.cssSelector("table#example > tbody > tr:nth-child(5)")); //css
        row = driver.findElement(By.xpath("//table[@id='example']/tbody/tr[5]")); //xpath
        //System.out.println("ROW 5: " + row.getText());

        //Particular column
        columns = driver.findElements(By.cssSelector("table#example > tbody td:nth-child(3)")); //css
        columns = driver.findElements(By.xpath("//table[@id='example']/tbody/tr/td[5]")); //xpath
        //System.out.println("COLUMN 3: " + columns);

        //Particular cell
        cell = driver.findElement(By.cssSelector("table#example > tbody tr:nth-child(2) td:nth-child(2)")); //css
        cell = driver.findElement(By.xpath("//table[@id='example']/tbody/tr[2]/td[2]")); //xpath
        //System.out.println("CELL 2-2: " + cell.getText());

        context.setAttribute("nameAllRows", "Teb All Rows");
        context.setAttribute("nameAllCellsFromRow", "Teb all Cells");
    }

    @Test
    public void getAllRows(ITestContext context) {
        driver.get(baseUrl + "/table-sort-search-demo.html");

        System.out.println(context.getAttribute("nameAllRows"));

        List<WebElement> rows = driver.findElements(By.cssSelector("table#example > tbody > tr"));
        for (WebElement row: rows) {
            if(row.getText().contains("Software Engineer"))
            System.out.println(row.getText());
        }
    }

    @Test
    public void getAllCellsFromARow(ITestContext context) {
        driver.get(baseUrl + "/table-sort-search-demo.html");

        System.out.println(context.getAttribute("nameAllCellsFromRow"));

        List<WebElement> cells = driver.findElements(By.cssSelector("table#example > tbody tr:nth-child(2) > td"));
        for (WebElement cell : cells) {
            System.out.println(cell.getText());
        }
    }

    @AfterClass
    public void close() throws InterruptedException {
        Thread.sleep(3000);
        driver.close();
    }

}
