package data;


import com.github.javafaker.Faker;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BasicInformation {
    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String state;
    private String zipCode;
    private String birthDate;

    public BasicInformation() {
        Faker faker = new Faker();
        firstName = faker.name().firstName();
        lastName = faker.name().lastName();
        address = faker.address().streetAddress();
        city = faker.address().city();
        zipCode = faker.address().zipCode();
        state = "NY";
        birthDate = "09/10/1985";
    }
}