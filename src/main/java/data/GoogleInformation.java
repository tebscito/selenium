package data;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GoogleInformation {

    private String fistButton;
    private String lastButton;

}