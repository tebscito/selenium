import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ZeroTest {
    private WebDriver driver;
    private String baseUrl = "http://zero.webappsecurity.com";


    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver" , "chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get(baseUrl);
        driver.manage().window().maximize();
    }

    @Test(enabled = false)
    public void zeroMainPageUI() {
        String expectedTitle = "Zero - Personal Banking - Loans - Credit Cards";
        String actualTitle = "";
        actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, expectedTitle, "Title page doesn't show as expected");

        //id, name, class name
        WebElement searchInputText_id = driver.findElement(By.id("searchTerm"));
        WebElement searchInputText_name = driver.findElement(By.name("searchTerm"));
        WebElement searchInputText_class = driver.findElement(By.className("search-query"));

        //link text, partial link
        WebElement homeLink = driver.findElement(By.linkText("Zero Bank"));
        WebElement homeLink_partial = driver.findElement(By.partialLinkText("Zero"));

        //css
        WebElement signinButton_css = driver.findElement(By.cssSelector("button#signin_button"));
        WebElement moreServices_css = driver.findElement(By.cssSelector("a.btn.btn-small.btn-info"));

        //xpath
        WebElement signinButton_xpath = driver.findElement(By.xpath("//button[@class='signin btn btn-info'][@type='button']"));

        Assert.assertTrue(searchInputText_id.isDisplayed(), "The search input text is not displayed");
        Assert.assertEquals(signinButton_css.getText(), "Signin", "The text of the buttton is different of expected");
        Assert.assertTrue(moreServices_css.isDisplayed(), "The button more service is not displayed");
    }

    @Test
    public void zeroNavigationPage_byMenu() {
        List<WebElement> menu = driver.findElements(By.cssSelector("ul[id='pages-nav'] li"));

        //Foreach
        for (WebElement item: menu) {
            String label = item.getText();
            System.out.println("MENU:" + label);
        }

        //Iterator
        Iterator<WebElement> iter = menu.iterator();
        while (iter.hasNext()) {
            WebElement item = iter.next();
            String label = item.getText();
            System.out.println("MENU:" + label);
        }
    }

    @Test
    public void zeroNavigationPage_byURL() {
        driver.navigate().to(baseUrl + "/index.html");
        System.out.println("Current URL: " + driver.getCurrentUrl());
        WebElement homeContent = driver.findElement(By.cssSelector("div[id='carousel'][class='carousel slide']"));
        Assert.assertTrue(homeContent.isDisplayed(), "The carousel is not displayed in HOME");

        driver.navigate().to(baseUrl + "/online-banking.html");
        System.out.println("Current URL: " + driver.getCurrentUrl());
        WebElement onlineBankingContent = driver.findElement(By.cssSelector("div[class='hero-unit hero-home']"));
        Assert.assertTrue(onlineBankingContent.isDisplayed(), "The carousel is not displayed in ONLINE BANKING");

        driver.navigate().to(baseUrl + "/feedback.html");
        System.out.println("Current URL: " + driver.getCurrentUrl());
        WebElement feedbackContent = driver.findElement(By.cssSelector("div[class='page-header']"));
        Assert.assertTrue(feedbackContent.isDisplayed(), "The carousel is not displayed in FEEDBACK");
    }

    @Test
    public void onlineBankingUI() {
        String expectedTitle = "Log in to ZeroBank";
        driver.navigate().to(baseUrl + "/online-banking.html");
        driver.findElement(By.cssSelector("a[href='/login.html']")).click();
        WebElement pageTitle = driver.findElement(By.className("page-header"));
        WebElement userInputText = driver.findElement(By.id("user_login"));
        WebElement passwordInputText = driver.findElement(By.id("user_password"));
        WebElement signInButton = driver.findElement(By.name("submit"));
        Assert.assertEquals(pageTitle.getText(), expectedTitle);
        Assert.assertTrue(userInputText.isDisplayed());
        Assert.assertTrue(passwordInputText.isDisplayed());
        Assert.assertTrue(signInButton.isDisplayed());
    }

    @Test
    public void feedbackUI() {
        //Declare test data variables
        String name = "Juan Carlos";
        String email = "juan.carlos@gmail.com";
        String subject = "Testing";
        String message = "Estamos aprendiendo selenium con Java";
        String expectedUrl = baseUrl + "/sendFeedback.html";
        String expectedFeedbackTitle = "Feedback";

        //Navigate to feedback page
        driver.navigate().to(baseUrl + "/feedback.html");

        //Get the elements from the UI - Feedback page
        WebElement nameInputText = driver.findElement(By.id("name"));
        WebElement emailInputText = driver.findElement(By.id("email"));
        WebElement subjectInputText = driver.findElement(By.id("subject"));
        WebElement messageInputArea = driver.findElement(By.id("comment"));
        WebElement sendMessageButton = driver.findElement(By.name("submit"));

        //Complete the form and send it
        nameInputText.sendKeys(name);
        emailInputText.sendKeys(email);
        subjectInputText.sendKeys(subject);
        messageInputArea.sendKeys(message);
        sendMessageButton.click();

        //Get the elements from the UI - SendFeedback page
        WebElement feedbackTitle = driver.findElement(By.id("feedback-title"));
        String sendFeedbacklUrl = driver.getCurrentUrl();

        //Check the expected behaviour (current url and page title)
        Assert.assertEquals(sendFeedbacklUrl, expectedUrl);
        Assert.assertEquals(feedbackTitle.getText(), expectedFeedbackTitle);
    }

    @AfterClass
    public void close() {
        driver.close();
    }
}
